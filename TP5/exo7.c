#include<stdio.h>
#include<stdlib.h>
#define SIZE 256

int readLine(char* file)
{
    FILE* fichier = fopen(file,"r");
    int num,k=0;
    if(fichier == NULL)
        return -1;
    char *buff = malloc(SIZE*sizeof(char));
    fseek(fichier,5,SEEK_CUR);
    fgets(buff,SIZE,fichier);
    printf("Nom de la ligne : %s",buff);
    fscanf(fichier,"%[^ ] %d ",buff,&num);
    printf("Nombre d'arrêt(s) : %d \n",num);
    while(!feof(fichier)){
        fscanf(fichier,"%[^\n]\n",buff);
        if(k == num-1 || k == 0)
            printf("Terminus : %s\n",buff);
        k++;
    }
    free(buff);
    fclose(fichier);
    return 0;
}
int main ( int argc, char *argv[] )

{
    if(!argc)
        return -1;
    readLine(argv[1]);
    return 0;
}		

