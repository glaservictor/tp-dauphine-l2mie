#include<stdio.h>
#include<time.h>
#include<stdlib.h>


int buFree(char *file)
{
    FILE * fichier = fopen(file,"r"), *copy = fopen("copy_mise","w");
    if(fichier==NULL)
        return -1;
    int c = 0;
    while(c!=EOF)
    {
        c = fgetc(fichier);
        fputc((char)c,copy);
    }
    fclose(fichier);
    fclose(copy);
    return 0;
}

int wiBuff(char *file)
{

    FILE * fichier = fopen(file,"r"), *copy = fopen("copy_mise_buff","w");
    if(fichier==NULL)
        return -1;
    char *buff = malloc(32*sizeof(char));
    size_t res;
    do{
        res = fread(buff,sizeof(char),sizeof(buff),fichier);
        fwrite(buff,sizeof(char),sizeof(buff),copy);
        }while(res == sizeof(buff));
    fclose(copy);
    fclose(fichier);
    return 1;
}



int main ( int argc, char *argv[]) 
{
    if(argc==0)
        return -1;
    clock_t time_st,time_sto, time_sto1;
    time_st = clock();
    buFree(argv[1]);
    time_sto = clock();
    wiBuff(argv[1]);
    time_sto1 = clock();
    printf("Without buffer : %d \nWhith buffer %d\n",(int)(time_sto-time_st), (int)(time_sto1-time_sto));
    return 0;
}		

