#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<string.h>

#define SIZE 1024

int dim_File(char *file,int* w,int* h,int* vMax, int *mat[SIZE])
{
    char c = 0;
    int *cur = malloc(sizeof(int)),k=0;
    FILE *fichier = fopen(file,"r");
    if(fichier!=NULL)
    {
        fseek(fichier,3,SEEK_CUR);
        fscanf(fichier,"%d %d %d",h,w,vMax);
        for(int i = 0; i < *h; i++)
            for(int j = 0; j < *w; j++)
            fscanf(fichier,"%d",&mat[i][j]);
    }
    free(cur); 
    fclose(fichier);
    return 0;
}


void saveImage(int w, int h, int vmax,int *tab[h*w],char *file)
{
    FILE* fichier = NULL;
    fichier = fopen(file,"w");
    if(fichier!=NULL)
    {
        fprintf(fichier,"P2\n%d %d\n%d\n",h,w,vmax);
        for(int i = 0; i < h; i++){
            for(int j = 0; j < w; j++)
                fprintf(fichier,"%d ",tab[i][j]);
            fprintf(fichier,"\n");
        }
        fclose(fichier);
    }
}

void invImage(char* file)
{
    int h, w, vMax = 0;
    int *mat[SIZE];
    for(int i = 0; i < SIZE; i++)
        mat[i] = malloc(sizeof(int)*SIZE);
    dim_File(file,&w,&h,&vMax, mat);
    for(int i = 0; i< h; i++)
        for(int j = 0; j<w;j++)
            mat[i][j] = vMax -mat[i][j];
    char *name = malloc(sizeof(char)*255);
    sscanf(file,"%[^.]",name);
    sprintf(name,"%s-inv.pmg",name);
    saveImage(w,h,vMax,mat,name);
    for(int i = 0; i < SIZE; i++)
        free(mat[i]);
    free(name);
}
void blacWhite(char *file)
{
    int h, w, vMax = 0;
        int *mat[SIZE];
        for(int i = 0; i < SIZE; i++)
            mat[i] = malloc(sizeof(int)*SIZE);
        dim_File(file,&w,&h,&vMax, mat);
        for(int i = 0; i< h; i++)
            for(int j = 0; j<w;j++)
                mat[i][j] = (mat[i][j]>vMax/2 ? 0 : vMax);
        saveImage(w,h,vMax,mat,"bw.pgm");
        for(int i = 0; i < SIZE; i++)
            free(mat[i]);
}

void blur(char *file)
{
    int h, w, vMax = 0;
        int *mat[SIZE];
        for(int i = 0; i < SIZE; i++)
            mat[i] = malloc(sizeof(int)*SIZE);
        dim_File(file,&w,&h,&vMax, mat);
        for(int i = 0; i< h; i++)
            for(int j = 0; j<w;j++)
                if(i>1 && i < h-1 && j>1 && j<w-1)
                    mat[i][j] = (mat[i][j-1] + mat[i][j+1] + mat[i-1][j] + mat[i+1][j])/4;
        saveImage(w,h,vMax,mat,"flou.pgm");
        for(int i = 0; i < SIZE; i++)
            free(mat[i]);
}

int main ( int argc, char *argv[] )

{
    if(argc==0)
        return EXIT_FAILURE;
        invImage(argv[1]);
    return 0;
}		
