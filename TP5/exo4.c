#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<string.h>

#define SIZE 10000

int dim_File(char *file,int* w,int* h,int* vMax, int *mat[SIZE])
{
    char c = 0;
    int *cur = malloc(sizeof(int)),k=0;
    FILE *fichier = fopen(file,"r");
    if(fichier!=NULL)
    {
        fseek(fichier,3,SEEK_CUR);
        fscanf(fichier,"%d %d",h,w);
        while(!feof(fichier))
        {
            fscanf(fichier,"%d",cur);
                 if(*cur>*vMax)
                    *vMax = *cur;
            mat[k/(*w)][k%*w] = *cur;
            k++;
        }
    }
           
    fclose(fichier);
    return 0;
}


void saveImage(int w, int h, int vMax,int *tab[h*w],char *file)
{
    FILE* fichier = NULL;
    fichier = fopen(file,"w");
    if(fichier!=NULL)
    {
        fprintf(fichier,"P42");
        fprintf(fichier,"\n");
        fprintf(fichier,"%d %d",h,w);
        fprintf(fichier,"\n");
        for(int i = 0; i < w; i++){
            for(int j = 0; j < h; j++)
                fprintf(fichier,"%d ",tab[i][j]);
            fprintf(fichier,"\n");
        }
        fclose(fichier);
    }
}

int main ( int argc, char *argv[] )

{
    if(argc==0)
        return EXIT_FAILURE;
    int h, w, vMax = 0;
    int *mat[SIZE];
    for(int i = 0; i < SIZE; i++)
        mat[i] = malloc(sizeof(int)*SIZE);
    dim_File(argv[1],&w,&h,&vMax, mat);
    saveImage(w,h,vMax,mat,"test");
    return 0;
}		
