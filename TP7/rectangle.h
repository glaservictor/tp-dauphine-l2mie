#ifndef RECTANGLE_H
#define RECTANGLE_H

#include<stdio.h>

typedef struct {
  int x;
  int y;
  float L;
  float l;
} Rectangle;

Rectangle *creer_rectangle(int x, int y,int L,int l);

void afficher_rectangle(FILE* html, Rectangle* c);

int compare_rectangle(Rectangle *r1, Rectangle *r2);


void libere_rectangle(Rectangle* c);

#endif

