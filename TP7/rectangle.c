#include<stdio.h>
#include<stdlib.h>
#include"rectangle.h"


Rectangle* creer_rectangle(int x, int y,int L,int l)
{
    Rectangle *r = malloc(sizeof(Rectangle));
    r->l = l;
    r->L = L;
    r->x = x;
    r->y = y;
    return r;
}


void afficher_rectangle(FILE* html, Rectangle* c)
{
    fprintf(html," <rect x=%d y=%d width=%f height=%f stroke=’black’ fill=’black’ />",c->x,c->y,c->L,c->l);  
}

int compare_rectangle(Rectangle *r1, Rectangle *r2)
{
    if(r1->l*r1->L<r2->l*r2->L)
        return -1;
    return 1;
}

void libere_rectangle(Rectangle* c)
{
       free(c);
}

