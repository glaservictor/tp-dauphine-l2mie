#ifndef VOITURE_H
#define VOITURE_H

#include"cercle.h"
#include"rectangle.h"

typedef struct {
  cercle* avant;
  cercle* arriere;
  Rectangle* bas;
  Rectangle* haut;
  Rectangle* fenetre_avant;
  Rectangle* fenetre_arriere;
} voiture;

typedef voiture voiture_t;

voiture* creer_voiture(cercle* avant,cercle* arriere,Rectangle* bas,Rectangle* haut,Rectangle* f1,Rectangle* f2);

/* Prends en entree un voiture et un fichier HTML ouvert en écriture et ecrit dessine une voiture selon ses champs dans le fichier */
void afficher_voiture(FILE* html, voiture* v);

/* Libere tout l'espace mémoire utilise pour stocker une voiture */
void libere_voiture(voiture* v);

int compare_voiture(voiture *v1,voiture *v2);

#endif
