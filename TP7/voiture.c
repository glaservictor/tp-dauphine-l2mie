#include<stdio.h>
#include<stdlib.h>
#include"voiture.h"
#include"cercle.h"
#include"rectangle.h"


voiture* creer_voiture(cercle* avant,cercle* arriere,Rectangle* bas,Rectangle* haut,Rectangle* f1,Rectangle* f2)
{
    voiture_t *voiture = malloc(sizeof(voiture_t));
    voiture->arriere = arriere;
    voiture->avant = avant;
    voiture->bas = bas;
    voiture->haut = haut;
    voiture->fenetre_arriere =f2;
    voiture->fenetre_avant= f1;
    return voiture;
}


int nbV = 0;
void afficher_voiture(FILE* html, voiture_t *v)
{
    fprintf(html, "<g transform=\"translate(0, %d)\">", nbV*50);
    afficher_rectangle(html,v->bas);
    afficher_rectangle(html,v->fenetre_avant);
    afficher_rectangle(html,v->haut);
    afficher_rectangle(html,v->fenetre_arriere);
    afficher_cercle(html,v->avant);
    afficher_cercle(html,v->arriere);
    nbV++;
}

int compare_voiture(voiture *v1,voiture *v2)
{
    if(compare_rectangle(v1->bas,v2->bas)<0)
        return -1;
    return 1;
}

void libere_voiture(voiture* v)
{
    free(v);
}
