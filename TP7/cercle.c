#include"cercle.h"
#include<stdio.h>
#include<stdlib.h>
#define PI 3.14


cercle* creer_cercle(int x, int y, double r){
  cercle* res = malloc(sizeof(cercle));
  if(res==NULL) return NULL;
  res->x=x;
  res->y=y;
  res->r=r;
  return res;
}

void afficher_cercle(FILE* html, cercle* c){
   fprintf(html, "<circle cx='%d' cy='%d' r='%f' stroke='black' fill='grey' />\n", c->x, c->y, c->r);
}

int compare_cercle(cercle *c1, cercle *c2)
{
    if(PI*c1->r<PI*c2->r)
        return -1;
    return 1;
}

void libere_cercle(cercle* c) {
  free(c);
}
