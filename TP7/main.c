#include<stdio.h>
#include<stdlib.h>
#include"rectangle.h"
#include"cercle.h"
#include"voiture.h"

int main(void) {

  cercle* avant=creer_cercle(50,50,6);
  cercle* arriere=creer_cercle(100,50,7);
  Rectangle* bas=creer_rectangle(40,30,70,15);
  Rectangle* haut=creer_rectangle(60,15,40,25);
  Rectangle* f1=creer_rectangle(70,25,10,15);
  Rectangle* f2=creer_rectangle(80,25,10,15);
  voiture* ferrari=creer_voiture(avant,arriere,bas,haut,f1,f2);
   FILE* html = fopen("voiture.html", "w");
   if(html==NULL) {
      printf("Error opening\n");
      return 0;
   }
   fprintf(html, "<!DOCTYPE html>\n<html>\n<body>\n");
   fprintf(html, "<svg height=\"800\" width=\"1000\">");
   afficher_voiture(html,ferrari);
   fprintf(html, "\n</svg></body>\n</html>");
   libere_voiture(ferrari);
   fclose(html);
}

