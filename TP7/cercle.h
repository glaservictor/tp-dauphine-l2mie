#ifndef CERCLE_H
#define CERCLE_H

#include<stdio.h>

typedef struct {
  int x;
  int y;
  double r;
} cercle;

/* Initialise une variable cercle aux valeurs passees en entree*/
cercle* creer_cercle(int x, int y, double r);

/* Prends en entree un cercle et un fichier HTML et dessine un cercle dans le fichier HTML selon valeur des champs du cercle */
void afficher_cercle(FILE* html, cercle* c);

int compare_cercle(cercle *c1,cercle *c2);

/* desalloue le cercle de la memoire */
void libere_cercle(cercle* c);

#endif
