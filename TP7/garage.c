#include<stdio.h>
#include"voiture.h"
#include"rectangle.h"
#include"cercle.h"
#include<stdlib.h>


int SnD(voiture *v,int n)
{
    int index = 0;
    for(int i = 0; i < n; i++)
        if(compare_voiture(&v[index],&v[i])>0)
                index = i;
    return index;
}
    
voiture* selection_sort(voiture *v, int n)
{
    int index = 0;
    for(int i = 0; i < n; i++)
    {
        index = i;
        for(int j = i; j < n ; j++)
            if(compare_voiture(&v[index],&v[j])>0)
                index = j;
        voiture cop = v[i];
        v[i] = v[index];
        v[index] = cop;
    }
    return v;
}

voiture* creer_voiture_cabriolet(){
    cercle* avant=creer_cercle(50,50,6);
    cercle* arriere=creer_cercle(100,50,7);
    Rectangle* bas=creer_rectangle(40,30,70,15);
    Rectangle* haut=creer_rectangle(60,15,40,25);
    Rectangle* f1=creer_rectangle(70,25,10,15);
    Rectangle* f2=creer_rectangle(80,25,10,15);
    voiture* cabriolet=creer_voiture(avant,arriere,bas,haut,f1,f2);
    return cabriolet;
}


voiture* creer_voiture_familiale(){
    cercle* avant=creer_cercle(60,50,10);
    cercle* arriere=creer_cercle(110,50,12);
    Rectangle* bas=creer_rectangle(40,20,80,20);
    Rectangle* haut=creer_rectangle(50,15,60,25);
    Rectangle* f1=creer_rectangle(70,25,10,15);
    Rectangle* f2=creer_rectangle(80,25,10,15);
    voiture* cabriolet=creer_voiture(avant,arriere,bas,haut,f1,f2);
    return cabriolet;
}


void cree_garage(voiture *v)
{
    int rand;
    for(int i = 0; i < 10; i++)
    {
        rand = random()%100;
        if(rand%2)
            v[i] = *creer_voiture_cabriolet();
        else
            v[i] = *creer_voiture_familiale();
    }
}


