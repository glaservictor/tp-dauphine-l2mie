#include<stdio.h>

typedef struct Complexe{
    int re;
    int im; 
}Complexe;

void RemplisComplexe(Complexe *z)
{
    printf("Partie Réelle et Imaginaire : \n");
    scanf("%d %d",&z->re,&z->im);
}
// Avec le const on écrit sur un objet constant mais c'est l'adresse du pointeur et non les valeurs contenue dedans donc cela marchera avec un warning
// CONCLUSION on ne peut pas déclarer une structure en constant dans une fonction il faut le faire dans sa decla initiale


void AfficheComplexe(const Complexe *z)
{
    printf("Re(z) = %d | Im(z) = %d | &z = %p ",z->re, z->im, z);
}


int EgaliteComplexe(const Complexe *z1, const Complexe *z2)
{
    return (z1->re==z2->re && z1->im==z2->im);
}

Complexe SommeComplexe(const Complexe *z1, const Complexe *z2)
{
    Complexe z;
    z.re = z1->re + z2->re;
    z.im = z1->im + z2->im;
    return z;
}

int main ( int argc, char *argv[] )

{
    Complexe z1 = {4,4},z2 = {4,4},z;
    RemplisComplexe(&z1);
    if(EgaliteComplexe(&z1, &z2))
            printf("Ils sont égaux ! ");
    z = SommeComplexe(&z1,&z2);
    AfficheComplexe(&z);
    return 0;
}		
