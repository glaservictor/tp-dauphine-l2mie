#include<stdio.h>
#define SIZE 1000

typedef struct Complexe{
    int re;
    int im; 
}Complexe;

void RemplisComplexe(Complexe *z)
{
    printf("Partie Réelle et Imaginaire : \n");
    scanf("%d %d",&z->re,&z->im);
}
// Avec le const on écrit sur un objet constant mais c'est l'adresse du pointeur et non les valeurs contenue dedans donc cela marchera avec un warning
// CONCLUSION on ne peut pas déclarer une structure en constant dans une fonction il faut le faire dans sa decla initiale


void AfficheComplexe(const Complexe *z)
{
    printf("Re(z) = %d | Im(z) = %d | &z = %p ",z->re, z->im, z);
}


int EgaliteComplexe(const Complexe *z1, const Complexe *z2)
{
    return (z1->re==z2->re && z1->im==z2->im);
}

Complexe SommeComplexe(const Complexe *z1, const Complexe *z2)
{
    Complexe z;
    z.re = z1->re + z2->re;
    z.im = z1->im + z2->im;
    return z;
}


typedef struct EnsembleComplexes
{
    Complexe tab[SIZE];
    int size;
}EnsembleComplexes;

void InstancieEnsembleComplexe(EnsembleComplexes *Ez)
{
    printf("Number of numbers ;) : \n");
    scanf("%d",&Ez->size);
    for(int i = 0; i < Ez->size; i++)
        RemplisComplexe(&Ez->tab[i]);  
}

void AfficheEnsembleComplexe(EnsembleComplexes *ez)
{
    for(int i = 0; i < ez->size; i++)
    {
        AfficheComplexe(&ez->tab[i]);
        printf("\n");
    }
}

int main ( int argc, char *argv[] )

{
    EnsembleComplexes ez;
    InstancieEnsembleComplexe(&ez);
    AfficheEnsembleComplexe(&ez);
    return 0;
}		
