#include<stdio.h>
#include<string.h>
#define SIZE 256

typedef struct Etudiant{
    char nom[SIZE];
    char prenom[SIZE];
    int numero;
} Etudiant;

Etudiant CreerEtudiant(char* p,char* n,double num)
{
    Etudiant res;
    res.numero = num;
    size_t slen = strlen(n);
    if(slen<SIZE-1)
        strncpy(res.nom,n,slen);
    res.nom[slen] = '\0';
    strcpy(res.prenom,p);
    return res;
}
void AfficherEtudiant(const Etudiant* e)
{
    printf("%d %s %s\n",e->numero,e->prenom,e->nom);
}
int main()
{
    Etudiant e1;
    Etudiant e2;
    e1 = CreerEtudiant("Nada","Mimouni",2);
    AfficherEtudiant(&e1);
    e2 = CreerEtudiant("Ernest-Antoine","Seilliere",1);
    AfficherEtudiant(&e2);
    return 0;
}

