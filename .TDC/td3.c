#include<stdio.h>
#define T_MAX 256 


void Copie(char ch1[], char ch2[])
{
    int i = 0;
    while(ch1[i] != '\0')
       {
           i++;
           ch2[i] = ch1[i];
       }
}

void display(char ch[]);

int Size(char ch[])
{
    int i = 0;
    while (ch[i] != '\0')
           i++;
    return i;
}


void Mirroir(char ch[])
{
    int i = Size(ch);
    char cop;
       for(int j = 0; j < i ; j++)
        {
        cop = ch[j];
        ch[j] = ch[i-1-j];
        ch[i-j] = cop;
        }
}

void SupprOccur(char ch[], char c)
{
    int  i = 0;
    int cur = 0;
    while( ch[i] != '\0')
    {
        if(ch[i] != c)
        {
            ch[cur] = ch[i];
            cur++;
        }
        i++;
    }
    ch[cur] = '\0';
}


void display(char ch[])
{
    int i  = 0;
    while(ch[i] != '\0')
        {
            printf("%c",ch[i]);
            i++;
        }
}

int estPalindrome(char ch[])
{
    int size = Size(ch), i = 0;
    char ch1[size];
    Copie(ch,ch1);
    Mirroir(ch1);
    SupprOccur(ch,' ');
    SupprOccur(ch1,' ');
    while(ch[i] != '\0')
    {
        if(ch[i]!=ch1[i])
            return 0;
        i++;
    }
    return 1;
}


void RemplirTableMult(int tab[][T_MAX])
{
    for(int i = 0; i <= 10;i++)
        for(int j = 0; j <= 10; j++)
            tab[i][j] = i*j;
}

void AfficheTabInt(int tab[][T_MAX])
{
    for(int i = 0; i < 10; i++)
    {
        for(int j = 0; j<10;j++)
            printf("%d\t",tab[i][j]);
        printf("\n");
    }
}



void CreerDamier(char tab[][T_MAX],int n)
{
    for(int i = 0; i < n; i++){

        for(int j = 0; j < n; j++)
        {
            if((i+j)%2)
                tab[i][j] = '*';
            else 
                tab[i][j] = '-';
            printf("%c",tab[i][j]);
        }
        printf("\n");
    }
}

void CreerCroix(char tab[][T_MAX], int n, int i, int j)
{
    if( i< 0 || i >= n || j <0 || j>=n)
        return;
    for(int m = 0; m < n; m++){
        for ( int p = 0; p < n ; p++ ) {
            if(i == m && j == p) 
                tab[m][p] = '*';
            else if(m-i == p-j|| i+j == p+m)
                tab[m][p] = '+';
            else 
                tab[m][p] = '.';
            printf("%c",tab[m][p]);
        }
        printf("\n");
    }
}



int main ( int argc, char *argv[] )
{
    char tab[T_MAX][T_MAX];
    //CreerCroix(tab,100, 20, 59);
    return 0;
}		
