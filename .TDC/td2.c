#include<stdio.h>
#define size  9

int search(int tab[], int n,int k)
{
    int count = 0;
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n;j++)
        {
            if(tab[i] + tab[j] == k)
            count++;
        }
    count/=2;
    return count;
}

int search_sup(int tab[], int n, int k)
{
    int max = tab[0],count = 0;
    for(int i = 1; i < size; i++)
        if(tab[i-1]<tab[i])
                max = tab[i];
    int tab2[max];
    for(int i = 0; i < max; i++)
        tab2[i] = -1;
    for(int i = 0; i < size; i++)
        tab2[tab[i]] = i;
    for(int i = 0; i<max/2+1; i++)
        if(tab2[i]!=-1 && tab2[k-i]!=-1 && k-i!=i)
            count++;
    count/=2;
    return count;

}
int main()
{

    int sum = 5;
    int tab[size]={1,2,3,4,5,6,7,8,9};
    printf("Il y a %d élément qui valent : %d\n",search_sup(tab,size,sum),sum);
    return 0;
}
