#include<stdio.h>
#include<string.h>
#define SIZE 100


typedef struct Rationnel {
    int num;
    int den;
    char sig; //0 si négatif 1 si positif 
}Rationnel;

void RemplirRationnel(Rationnel *r)
{   
    do{
    printf("Saisissez votre rationnel d'abord le numérateur, le denominateur puis 0  pour négatif ou 1 pour positif : \n");
    scanf("%d %d %c",&r->num, &r->den,&r->sig);
    }while(!r->den);
    r->sig-=48;
}
    
void AfficheRationnelle(Rationnel *r)
{
    if(!r->sig)
        printf("-");
    printf("%d/%d", r->num,r->den);
}
    

Rationnel MultiplieRationnel(const Rationnel *r1,const  Rationnel *r2)
{
    Rationnel res = {0,1,1};
    res.num = r1->num*r2->num;
    res.den = r2->den*r1->den;
    if((r1->sig || r2->sig) && (!r1->sig || !r2->sig ))
        res.sig = 0;
    return res;
}

typedef struct Etudiant
{
    char nom[SIZE];
    int num;
    int notes[SIZE];
    int size;
}Etudiant;



void SaisieEtudiant(Etudiant *e)
{
    printf("Tapez nom, numéro étudiants son nombre et les valeurs de ses notes \n");
    scanf("%s %d %d",e->nom,&e->num, &e->size);
    for(int i = 0; i < e->size; i++)
        scanf("%d",&e->notes[i]);
}


void AfficheEtudiant(Etudiant *e)
{
    printf("Nom : %s \n",e->nom);
    printf("Numéro Etudiant %d ",e->num);
    for(int i = 0; i < e->size; i++)
        printf("Notes n°%d : %d \n",i, e->notes[i]);
}

int Moyenne(Etudiant *e)
{
    float moyenne = 0;
    if(!(e->size<SIZE))
        return 0;
    for(int i = 0; i < e->size; i++)
        moyenne += e->notes[i];
    moyenne/=e->size;
    return 1;
}


int AjoutNotes(Etudiant *e, int note)
{
    if(e->size == SIZE-1)
        return 0;
    e->notes[e->size] = note;
    e->size++;
    return 1;
}

typedef struct Classe 
{
    Etudiant classe[SIZE];
    int size;
}Classe;

void AfficheClasse(Classe *c)
{
    for(int i = 0; i < c->size; i++)
        AfficheEtudiant(&c->classe[i]);
}

int AjoutEtudiant(Classe *c,Etudiant *e)
{
    if(c->size<SIZE)
    {
        c->classe[c->size] = *e;
        return 1;
    }
    return -1;
}

int MoyenneEtudiant(char* nom, Classe *c)
{
    for(int i = 0; i < c->size; i++)
    {
        if(!strcmp(nom,c->classe[i].nom))
            return Moyenne(&c->classe[i]);
    }
    return -1;
}

Etudiant MeilleureMoyenne(Classe *c)
{
    int maxMoy = -1,index = -1;
    for(int i = 0; i < c->size; i++)
        if(maxMoy<Moyenne(&c->classe[i]))
            index = i;
         return c->classe[index];
 }
       

int main ()

{
    return 0;
}		

