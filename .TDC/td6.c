#include<stdio.h>
#include<stdlib.h>

typedef struct Ensemble{
    int card;
    int size;
    int *tab;
}Ensemble;

Ensemble* InitEnsemble()
{
    Ensemble *e = {0};
    e->size = 5;
    e->card = 0;
    e->tab = malloc(e->size*(sizeof(int)));
    e = (Ensemble*)malloc(5*sizeof(Ensemble));
    return e;
}

int BelongEnsemble(int x,Ensemble* e)
{
    for(int i = 0; i < e->card; i++)
        if(x==e->tab[i])
            return 1;
    return 0;
}

void AjouteElement(int x, Ensemble *e)
{
    for(int i = 0; i < e->card; i++)
        if(e->tab[i] == x)
            return;
    if(e->card == e->size)
        e->tab = realloc(e,2*sizeof(Ensemble));
    e->tab[e->card+1]=x;
}

void DeletionElement(int x, Ensemble *e)
{
    int index = 0;
    for(int i = 0; i < e->card; i++)
        if(i==x)
        {
            index = i;
            continue;
        }
    e->tab[index] = e->tab[e->card];
    e->card--;
}

void DesalloueEnsemble(Ensemble *e)
{
        free(e->tab);
        free(e);
}

Ensemble* UnionEnsemble(Ensemble *X,Ensemble *Y)
{
    Ensemble *Z = InitEnsemble();
    for(int i = 0; i < X->card; i++)
        AjouteElement(X->tab[i],Z);
    for(int j = 0; j < Y->card;j++)
        AjouteElement(Y->tab[j],Z);
    return Z;
}

Ensemble *InterEnsemble(Ensemble *X,Ensemble *Y)
{
    Ensemble *Z = InitEnsemble();
    for(int i = 0; i < X->card; i ++)
        for(int j = 0; j < Y->card; j++)
            if(X->tab[i]==Y->tab[j])
                AjouteElement(X->tab[i],Z);
    return Z;
}



