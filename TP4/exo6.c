#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<limits.h>
#include<sys/queue.h>

// A MODIFIER LA CONDITION D'ARRET REFLECHIR A UNE STRUCTURE VERIFIANT LA FORMATION DE BOUCLE OU D'UNE STRUCTURE SUP

int sour(int V, int Va, int Vb,int *max,int *count)
{
    int a = pow(Va,3)+pow(Vb,2), b = pow(Va,2)+pow(Vb,3);
    if(a < V && a > *max)
        *max = a;
    if(b < V && b > *max)
        *max = b;
    if(a<V  && (int)sqrt(pow(Va,3))!=Va)
    {
        ++*count;
        return sour(V,pow(Va,3),pow(Vb,2),max,count);
    }
    if(b<V  && (int)sqrt(pow(Vb,3)) != Vb)
    {
        ++*count;
        return sour(V,pow(Va,2),pow(Vb,3),max,count);
    }
    else if(sqrt(Va) + sqrt(Vb) < V && (int)sqrt(Va)>1 && (int)sqrt(Vb)>1 && *count<10000)
        return sour(V,sqrt(Va),sqrt(Vb),max,count);
    return *max;
}
int main ( int argc, char *argv[] )

{
    int max = 0,count = 0; 
    sour(301,14,7,&max,&count);
    printf("MAX : %d\n",max);
    return 0;
}		
