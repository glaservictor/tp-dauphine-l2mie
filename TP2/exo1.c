#include<stdio.h>
#include<time.h>
#define N 50

void Remplissage(int tab[], int n)
{
  for (int i = 0; i < n; i++)
    tab[i] = rand()% 10;
}

void Affichage(int tab[], int n)
{
    for(int i = 0; i < n; i++)
        printf("%d",tab[i]);
    printf("\n");
}

int Position(int tab[], int n, int e)
{
    for(int i = 0; i < n; i++)
        if(tab[i]==e)
            return i;
    return -1;
}

void Somme(int t1[], int t2[], int t3[], int n)
{
    for(int i = 0; i < n; i++)
        t3[i] = t2[i] + t1[i];
}


int main() {
    int tab1[N], tab2[N], tab3[N];
    int nb;
    srandom(time(NULL));
    do
    {
         printf("Entrez le nombre de valeurs que vous d ́esirez\n");
         scanf("%d", &nb);
    }
    while(nb>N);
    Remplissage(tab1, nb);
    Affichage(tab1, nb);
    printf("%d %d\n", Position(tab1, nb, 2), Position(tab1, nb, 4));
    Remplissage(tab2, nb);
    Somme(tab1, tab2, tab3, nb);
    Affichage(tab3, nb);
    return 0;
}
