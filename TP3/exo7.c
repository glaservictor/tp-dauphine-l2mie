#include<stdio.h>
#include<string.h>
#define N 256


int Joker(char s1[], char s2[])
{
    int tab[26] = {0};
    for(int i = 0; i < strlen(s1);i++)
        tab[s1[i]] = 1;
    for(int j = 0; j < strlen(s2); j++)
        if(tab[s2[j]])
            return j;
    return -1;
}


int Size(char s[])
{
    int i = 0,size = 0;
    while(s[i] !='\0')
    {   
        if(s[i] != 0)
            size = i;
        i++;
    }
    return ++size;
}


void concat (char s1[N], char s2[N])
{
    int size = Size(s1);
    for(int i = size ; i < size  + Size(s2) +1 ; i++)
        s1[i] = s2[i - size];
}

int EndString(char s[])
{
    int i = 0,count = 0;
    while(s[i] != '\0')
    {
        if(s[i] != ' ')
            count = i;
        i++;
    }
    if(s[++count] !='\0')
        return ++count;
    return count;
}


void PigLatin(char s1[], char s2[])
{
    strcpy(s2,s1); 
    char trans;
    char voyelle[] = {'a','e', 'i', 'o', 'u', 'y'};
    for(int i = 0; i < 5; i++)
    {
        if(voyelle[i] == s1[0])
        {
            concat(s2,"way"); 
            return;
        }
    }
    int firstVoy = Joker(voyelle,s2);
    if(firstVoy != -1)
    {
        int end = EndString(s2);
        for(int i = firstVoy; i < end; i++)
        {
            trans = s2[i];
            s2[i] = s2[i-firstVoy];
            s2[i-firstVoy] = trans;;
        }
    }
    concat(s2,"ay");
}


int main ( int argc, char *argv[] )

{
    char s1[N] = "string";
    char s2[N];
    PigLatin(s1,s2);
    printf("%s",s2);
    return 0;
}		
