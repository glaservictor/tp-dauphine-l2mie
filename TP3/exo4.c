#include<stdio.h>
#define T_MAX 256

int Size(char s[])
{
    int i = 0;
    while(s[i] != '\0')
        i++;
    return i;
}

int Anagramme(char s1[], char s2[])
{
    char as[T_MAX];
    if(Size(s1) != Size(s2))
        return 0;
    for(int i = 0; i < Size(s1); i++)
        {
            as[s1[i]]++;
            as[s2[i]]--;
        }
    for(int i = 0; i < Size(s1); i++)
        if(as[i] != 0)
            return 0;
    return 1;
}

int main ( int argc, char *argv[] )

{
    char s1[] = "TSARINE";
    char s2[] = "ENTRAIS";
    if(Anagramme(s1,s2))
            printf("Ca marche ! \n");
    return 0;
}		

