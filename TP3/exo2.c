#include<stdio.h>


int remplace(char s[], char c, char r)
{
    int i = 0,count = 0;
    while(s[i] != '\0')
    {
        if(s[i] == c)
        {
            s[i] = r;
            count++;
        }
        i++;
    }
    return count;
}

int main ( int argc, char *argv[] )

{
    char ts[] = "Hello";
    int c = remplace(ts,'l','k');
    printf("%s changements : %d \n",ts,c); 
    return 0;
}		

